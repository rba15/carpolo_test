package com.carpolo.roaa.carpolo_test.realm_objects;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 *
 * Class that defines the Point schema in Realm
 *
 */
public class RealmPoint extends RealmObject {

    @PrimaryKey
    private String coordinates; // latitude,longitude
    private double latitude;
    private double longitude;
    private String name; // name of the location (point)

    public RealmPoint(){

    }
    
    public RealmPoint(String coordinates, double latitude, double longitude, String name) {
        this.coordinates = coordinates;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}