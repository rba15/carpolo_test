package io.realm;


public interface RealmRouteRealmProxyInterface {
    public long realmGet$id();
    public void realmSet$id(long value);
    public String realmGet$origin();
    public void realmSet$origin(String value);
    public String realmGet$destination();
    public void realmSet$destination(String value);
    public String realmGet$routeString();
    public void realmSet$routeString(String value);
    public java.util.Date realmGet$timeQueried();
    public void realmSet$timeQueried(java.util.Date value);
}
