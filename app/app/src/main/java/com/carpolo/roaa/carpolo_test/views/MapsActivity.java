package com.carpolo.roaa.carpolo_test.views;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.carpolo.roaa.carpolo_test.config.Configuration;
import com.carpolo.roaa.carpolo_test.classes.DirectionsJSONParser;
import com.carpolo.roaa.carpolo_test.R;
import com.carpolo.roaa.carpolo_test.classes.Point;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap map;
    private LocationManager locationmanager;
    final int REQUEST_ACCESS_LOCATION = 1;
    public static ArrayList<Point> points; // our 2 points
    public static String route = ""; // route between the two points

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Callback function when map is loaded
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        addPoints(); // add points to map
        getUserLocation(); // request user location
        addRoute(); // add path between two points
    }

    /**
     * Function that gets user's current location
     */
    public void getUserLocation(){

        // check if app has permission to access location
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            // request permission to access location
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_LOCATION);

        else {
            // permission granted, get user's location
            locationmanager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            String provider = locationmanager.getBestProvider(new Criteria(), false);

            if (provider != null & !provider.equals("")) {

                Location location = locationmanager.getLastKnownLocation(provider);
                locationmanager.requestLocationUpdates(provider, 2000, 1, this);

                if (location != null)
                    onLocationChanged(location);

                else if(getApplicationContext() != null)
                    Toast.makeText(getApplicationContext(), "Current location not found", Toast.LENGTH_SHORT).show();
            }

            else if(getApplicationContext() != null)
                Toast.makeText(getApplicationContext(), "Current location not found", Toast.LENGTH_SHORT).show();

            map.setMyLocationEnabled(true);
        }
    }

    /**
     * Function that gets user's response to requested permission
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_ACCESS_LOCATION) {

            // permission granted
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                getUserLocation();
            } else { // permission denied
                Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Function that adds the points to the map
     */
    public void addPoints(){

        for(Point p : points){

            LatLng location = new LatLng(p.getLatitude(), p.getLongitude());
            map.addMarker(new MarkerOptions().position(location).title(p.getName()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(14).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    /**
     * Function that adds the route between the two points to the map
     */
    public void addRoute(){

        if(route.equals("")) { // no route found in Realm, fetch from google

            String url = setURL(points.get(0), points.get(1));
            RouteTask routeTask = new RouteTask();
            routeTask.execute(url);
        }

        else{// parse route found in database and add to map
            System.out.println("route in realm is: " + route);
            JSONParserTask parser = new JSONParserTask();
            parser.execute(route);
        }
    }

    /**
     * Function that sets the URL to the directions API
     * @param origin origin point of the route
     * @param destination destination point of the route
     * @return
     */
    private String setURL(Point origin, Point destination) {

        String originParam = "origin=" + origin.getLatitude() + "," + origin.getLongitude();
        String destParam = "destination=" + destination.getLatitude() + "," + destination.getLongitude();
        String sensorParam = "sensor=false";
        String modeParam = "mode=driving";

        // combine params
        String params = "key=" + Configuration.API_KEY +"&" + originParam + "&" + destParam + "&" + sensorParam + "&" + modeParam;

        // final url
        return Configuration.GOOGLE_SERVER + "?" + params;
    }

    /**
     * AsyncTask that fetches route from Directions API
     */
    private class RouteTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... str) {

            String result = "";

            try {

                // connect to directions api link
                URL url = new URL(str[0]);
                System.out.println("str is: " + str[0]);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // get response JSON
                InputStream inputStream = connection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                StringBuffer sb = new StringBuffer();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                result = sb.toString();

                Date currentTime = Calendar.getInstance().getTime();
                LoadingActivity.routeController.insertRoute(points.get(0),points.get(1),result, currentTime);

                br.close();
                inputStream.close();
                connection.disconnect();

            } catch (Exception e) {
                System.err.println("Exception: " + e.toString());
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            System.out.println("response is: " + result);

            if(result.equals("")) {
                Toast.makeText(getApplicationContext(), "Error Finding Route", Toast.LENGTH_SHORT).show();
                return;
            }

            // parse JSON response
            JSONParserTask parser = new JSONParserTask();
            parser.execute(result);
        }
    }

    /**
     * ASYNC task to parse the JSON route
     */
    private class JSONParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {

                // parse directions JSON using DirectionsJSONParser
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            if(result == null || result.size() == 0){

                Toast.makeText(getApplicationContext(), "Error Finding Route", Toast.LENGTH_SHORT).show();
                return;
            }

            // draw route on map

            ArrayList points = new ArrayList();
            PolylineOptions lineOptions = new PolylineOptions();

            List<HashMap<String, String>> path = result.get(0);

            for (int j = 0; j < path.size(); j++) {

                HashMap<String, String> point = path.get(j);

                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);

                points.add(position);
            }

            lineOptions.addAll(points);
            lineOptions.width(12);
            lineOptions.color(Color.parseColor("#80bfff"));
            lineOptions.geodesic(true);

            map.addPolyline(lineOptions);
        }
    }

    /**
     * Function that zooms to the user's current location when updated
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {

        // check permissions
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            return;

        if (map != null) {

            // get new location and zoom on it
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(14).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            locationmanager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
