package com.carpolo.roaa.carpolo_test.controllers;

import com.carpolo.roaa.carpolo_test.classes.Point;
import com.carpolo.roaa.carpolo_test.models.RouteModel;

import java.util.Date;

/**
 * Class that defines the Route Controller
 */
public class RouteController {

    private RouteModel routeModel;

    public RouteController() {
        this.routeModel = new RouteModel();
    }

    /**
     * Function that connects to the model to insert route between two points
     * @param origin origin point of the route
     * @param destination destination point of the route
     * @param route route from origin point to destination point
     */
    public void insertRoute(Point origin, Point destination, String route, Date timeQueried){
        routeModel.insertRoute(origin,destination,route, timeQueried);
    }

    /**
     * Function that connects to the model to fetch route between two points
     * @param origin origin point of the route
     * @param destination destination point of the route
     * @return route from origin point to destination point stored in Realm
     */
    public String getRoute(Point origin, Point destination){
        return routeModel.getRoute(origin,destination);
    }
}
