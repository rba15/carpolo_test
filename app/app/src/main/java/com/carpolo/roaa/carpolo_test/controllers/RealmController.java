package com.carpolo.roaa.carpolo_test.controllers;

import android.content.Context;
import com.carpolo.roaa.carpolo_test.models.RealmModel;

/**
 * Class that defines the Realm Controller
 */
public class RealmController {

    private RealmModel realmModel;

    public RealmController(Context appContext) {
        this.realmModel = new RealmModel(appContext);
    }
}
