package com.carpolo.roaa.carpolo_test.controllers;

import com.carpolo.roaa.carpolo_test.classes.Point;
import com.carpolo.roaa.carpolo_test.models.PointModel;

import java.util.ArrayList;

/**
 * Class that defines the Point Controller
 */
public class PointController {

    private PointModel pointModel;

    public PointController() {
        this.pointModel = new PointModel();
    }

    /**
     * Function that connects to the model to retrieve the points
     * @return ArrayList of the points in Realm
     */
    public ArrayList<Point> getPoints(){

        // if points in Realm fetch else create

        ArrayList<Point> points = this.pointModel.getPoints();

        if(points.size() != 0)
            return points;

        return this.insertMapPoints();
    }

    /**
     * Function that connects to the model to insert two points
     * @return ArrayList of the points inserted in Realm
     */
    public ArrayList<Point> insertMapPoints(){

        System.out.println("inserting map points");

        ArrayList<Point> points = new ArrayList<>();
        points.add(new Point("Lebanese American University", 33.8929 , 35.4779));
        points.add(new Point("American University of Beirut",33.9008, 35.4807));

        // insert these points into realm
        this.insertPoint(points.get(0));
        this.insertPoint(points.get(1));
        return points;
    }

    /**
     * Function that connects to the model to insert a point
     * @param p point to be inserted
     */

    public void insertPoint(final Point p){
        this.pointModel.insertPoint(p);
    }
}
