package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class RealmRouteRealmProxy extends com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute
    implements RealmObjectProxy, RealmRouteRealmProxyInterface {

    static final class RealmRouteColumnInfo extends ColumnInfo {
        long idIndex;
        long originIndex;
        long destinationIndex;
        long routeStringIndex;
        long timeQueriedIndex;

        RealmRouteColumnInfo(OsSchemaInfo schemaInfo) {
            super(5);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("RealmRoute");
            this.idIndex = addColumnDetails("id", objectSchemaInfo);
            this.originIndex = addColumnDetails("origin", objectSchemaInfo);
            this.destinationIndex = addColumnDetails("destination", objectSchemaInfo);
            this.routeStringIndex = addColumnDetails("routeString", objectSchemaInfo);
            this.timeQueriedIndex = addColumnDetails("timeQueried", objectSchemaInfo);
        }

        RealmRouteColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new RealmRouteColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final RealmRouteColumnInfo src = (RealmRouteColumnInfo) rawSrc;
            final RealmRouteColumnInfo dst = (RealmRouteColumnInfo) rawDst;
            dst.idIndex = src.idIndex;
            dst.originIndex = src.originIndex;
            dst.destinationIndex = src.destinationIndex;
            dst.routeStringIndex = src.routeStringIndex;
            dst.timeQueriedIndex = src.timeQueriedIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(5);
        fieldNames.add("id");
        fieldNames.add("origin");
        fieldNames.add("destination");
        fieldNames.add("routeString");
        fieldNames.add("timeQueried");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private RealmRouteColumnInfo columnInfo;
    private ProxyState<com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute> proxyState;

    RealmRouteRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (RealmRouteColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public long realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo.idIndex);
    }

    @Override
    public void realmSet$id(long value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$origin() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.originIndex);
    }

    @Override
    public void realmSet$origin(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.originIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.originIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.originIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.originIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$destination() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.destinationIndex);
    }

    @Override
    public void realmSet$destination(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.destinationIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.destinationIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.destinationIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.destinationIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$routeString() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.routeStringIndex);
    }

    @Override
    public void realmSet$routeString(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.routeStringIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.routeStringIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.routeStringIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.routeStringIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Date realmGet$timeQueried() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.timeQueriedIndex)) {
            return null;
        }
        return (java.util.Date) proxyState.getRow$realm().getDate(columnInfo.timeQueriedIndex);
    }

    @Override
    public void realmSet$timeQueried(Date value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.timeQueriedIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setDate(columnInfo.timeQueriedIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.timeQueriedIndex);
            return;
        }
        proxyState.getRow$realm().setDate(columnInfo.timeQueriedIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("RealmRoute", 5, 0);
        builder.addPersistedProperty("id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("origin", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("destination", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("routeString", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("timeQueried", RealmFieldType.DATE, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static RealmRouteColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new RealmRouteColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "RealmRoute";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute obj = null;
        if (update) {
            Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
            RealmRouteColumnInfo columnInfo = (RealmRouteColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
            long pkColumnIndex = columnInfo.idIndex;
            long rowIndex = Table.NO_MATCH;
            if (!json.isNull("id")) {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class), false, Collections.<String> emptyList());
                    obj = new io.realm.RealmRouteRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("id")) {
                if (json.isNull("id")) {
                    obj = (io.realm.RealmRouteRealmProxy) realm.createObjectInternal(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.RealmRouteRealmProxy) realm.createObjectInternal(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class, json.getLong("id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
            }
        }

        final RealmRouteRealmProxyInterface objProxy = (RealmRouteRealmProxyInterface) obj;
        if (json.has("origin")) {
            if (json.isNull("origin")) {
                objProxy.realmSet$origin(null);
            } else {
                objProxy.realmSet$origin((String) json.getString("origin"));
            }
        }
        if (json.has("destination")) {
            if (json.isNull("destination")) {
                objProxy.realmSet$destination(null);
            } else {
                objProxy.realmSet$destination((String) json.getString("destination"));
            }
        }
        if (json.has("routeString")) {
            if (json.isNull("routeString")) {
                objProxy.realmSet$routeString(null);
            } else {
                objProxy.realmSet$routeString((String) json.getString("routeString"));
            }
        }
        if (json.has("timeQueried")) {
            if (json.isNull("timeQueried")) {
                objProxy.realmSet$timeQueried(null);
            } else {
                Object timestamp = json.get("timeQueried");
                if (timestamp instanceof String) {
                    objProxy.realmSet$timeQueried(JsonUtils.stringToDate((String) timestamp));
                } else {
                    objProxy.realmSet$timeQueried(new Date(json.getLong("timeQueried")));
                }
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute obj = new com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute();
        final RealmRouteRealmProxyInterface objProxy = (RealmRouteRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((long) reader.nextLong());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'id' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("origin")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$origin((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$origin(null);
                }
            } else if (name.equals("destination")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$destination((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$destination(null);
                }
            } else if (name.equals("routeString")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$routeString((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$routeString(null);
                }
            } else if (name.equals("timeQueried")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$timeQueried(null);
                } else if (reader.peek() == JsonToken.NUMBER) {
                    long timestamp = reader.nextLong();
                    if (timestamp > -1) {
                        objProxy.realmSet$timeQueried(new Date(timestamp));
                    }
                } else {
                    objProxy.realmSet$timeQueried(JsonUtils.stringToDate(reader.nextString()));
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
        }
        return realm.copyToRealm(obj);
    }

    public static com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute copyOrUpdate(Realm realm, com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute) cachedRealmObject;
        }

        com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
            RealmRouteColumnInfo columnInfo = (RealmRouteColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
            long pkColumnIndex = columnInfo.idIndex;
            long rowIndex = table.findFirstLong(pkColumnIndex, ((RealmRouteRealmProxyInterface) object).realmGet$id());
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.RealmRouteRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute copy(Realm realm, com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute realmObject = realm.createObjectInternal(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class, ((RealmRouteRealmProxyInterface) newObject).realmGet$id(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        RealmRouteRealmProxyInterface realmObjectSource = (RealmRouteRealmProxyInterface) newObject;
        RealmRouteRealmProxyInterface realmObjectCopy = (RealmRouteRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$origin(realmObjectSource.realmGet$origin());
        realmObjectCopy.realmSet$destination(realmObjectSource.realmGet$destination());
        realmObjectCopy.realmSet$routeString(realmObjectSource.realmGet$routeString());
        realmObjectCopy.realmSet$timeQueried(realmObjectSource.realmGet$timeQueried());
        return realmObject;
    }

    public static long insert(Realm realm, com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
        long tableNativePtr = table.getNativePtr();
        RealmRouteColumnInfo columnInfo = (RealmRouteColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
        long pkColumnIndex = columnInfo.idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((RealmRouteRealmProxyInterface) object).realmGet$id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((RealmRouteRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((RealmRouteRealmProxyInterface) object).realmGet$id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$origin = ((RealmRouteRealmProxyInterface) object).realmGet$origin();
        if (realmGet$origin != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.originIndex, rowIndex, realmGet$origin, false);
        }
        String realmGet$destination = ((RealmRouteRealmProxyInterface) object).realmGet$destination();
        if (realmGet$destination != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.destinationIndex, rowIndex, realmGet$destination, false);
        }
        String realmGet$routeString = ((RealmRouteRealmProxyInterface) object).realmGet$routeString();
        if (realmGet$routeString != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.routeStringIndex, rowIndex, realmGet$routeString, false);
        }
        java.util.Date realmGet$timeQueried = ((RealmRouteRealmProxyInterface) object).realmGet$timeQueried();
        if (realmGet$timeQueried != null) {
            Table.nativeSetTimestamp(tableNativePtr, columnInfo.timeQueriedIndex, rowIndex, realmGet$timeQueried.getTime(), false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
        long tableNativePtr = table.getNativePtr();
        RealmRouteColumnInfo columnInfo = (RealmRouteColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute object = null;
        while (objects.hasNext()) {
            object = (com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((RealmRouteRealmProxyInterface) object).realmGet$id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((RealmRouteRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((RealmRouteRealmProxyInterface) object).realmGet$id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$origin = ((RealmRouteRealmProxyInterface) object).realmGet$origin();
            if (realmGet$origin != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.originIndex, rowIndex, realmGet$origin, false);
            }
            String realmGet$destination = ((RealmRouteRealmProxyInterface) object).realmGet$destination();
            if (realmGet$destination != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.destinationIndex, rowIndex, realmGet$destination, false);
            }
            String realmGet$routeString = ((RealmRouteRealmProxyInterface) object).realmGet$routeString();
            if (realmGet$routeString != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.routeStringIndex, rowIndex, realmGet$routeString, false);
            }
            java.util.Date realmGet$timeQueried = ((RealmRouteRealmProxyInterface) object).realmGet$timeQueried();
            if (realmGet$timeQueried != null) {
                Table.nativeSetTimestamp(tableNativePtr, columnInfo.timeQueriedIndex, rowIndex, realmGet$timeQueried.getTime(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
        long tableNativePtr = table.getNativePtr();
        RealmRouteColumnInfo columnInfo = (RealmRouteColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
        long pkColumnIndex = columnInfo.idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((RealmRouteRealmProxyInterface) object).realmGet$id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((RealmRouteRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((RealmRouteRealmProxyInterface) object).realmGet$id());
        }
        cache.put(object, rowIndex);
        String realmGet$origin = ((RealmRouteRealmProxyInterface) object).realmGet$origin();
        if (realmGet$origin != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.originIndex, rowIndex, realmGet$origin, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.originIndex, rowIndex, false);
        }
        String realmGet$destination = ((RealmRouteRealmProxyInterface) object).realmGet$destination();
        if (realmGet$destination != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.destinationIndex, rowIndex, realmGet$destination, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.destinationIndex, rowIndex, false);
        }
        String realmGet$routeString = ((RealmRouteRealmProxyInterface) object).realmGet$routeString();
        if (realmGet$routeString != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.routeStringIndex, rowIndex, realmGet$routeString, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.routeStringIndex, rowIndex, false);
        }
        java.util.Date realmGet$timeQueried = ((RealmRouteRealmProxyInterface) object).realmGet$timeQueried();
        if (realmGet$timeQueried != null) {
            Table.nativeSetTimestamp(tableNativePtr, columnInfo.timeQueriedIndex, rowIndex, realmGet$timeQueried.getTime(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.timeQueriedIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
        long tableNativePtr = table.getNativePtr();
        RealmRouteColumnInfo columnInfo = (RealmRouteColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute object = null;
        while (objects.hasNext()) {
            object = (com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((RealmRouteRealmProxyInterface) object).realmGet$id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((RealmRouteRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((RealmRouteRealmProxyInterface) object).realmGet$id());
            }
            cache.put(object, rowIndex);
            String realmGet$origin = ((RealmRouteRealmProxyInterface) object).realmGet$origin();
            if (realmGet$origin != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.originIndex, rowIndex, realmGet$origin, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.originIndex, rowIndex, false);
            }
            String realmGet$destination = ((RealmRouteRealmProxyInterface) object).realmGet$destination();
            if (realmGet$destination != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.destinationIndex, rowIndex, realmGet$destination, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.destinationIndex, rowIndex, false);
            }
            String realmGet$routeString = ((RealmRouteRealmProxyInterface) object).realmGet$routeString();
            if (realmGet$routeString != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.routeStringIndex, rowIndex, realmGet$routeString, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.routeStringIndex, rowIndex, false);
            }
            java.util.Date realmGet$timeQueried = ((RealmRouteRealmProxyInterface) object).realmGet$timeQueried();
            if (realmGet$timeQueried != null) {
                Table.nativeSetTimestamp(tableNativePtr, columnInfo.timeQueriedIndex, rowIndex, realmGet$timeQueried.getTime(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.timeQueriedIndex, rowIndex, false);
            }
        }
    }

    public static com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute createDetachedCopy(com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute) cachedObject.object;
            }
            unmanagedObject = (com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        RealmRouteRealmProxyInterface unmanagedCopy = (RealmRouteRealmProxyInterface) unmanagedObject;
        RealmRouteRealmProxyInterface realmSource = (RealmRouteRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());
        unmanagedCopy.realmSet$origin(realmSource.realmGet$origin());
        unmanagedCopy.realmSet$destination(realmSource.realmGet$destination());
        unmanagedCopy.realmSet$routeString(realmSource.realmGet$routeString());
        unmanagedCopy.realmSet$timeQueried(realmSource.realmGet$timeQueried());

        return unmanagedObject;
    }

    static com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute update(Realm realm, com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute realmObject, com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute newObject, Map<RealmModel, RealmObjectProxy> cache) {
        RealmRouteRealmProxyInterface realmObjectTarget = (RealmRouteRealmProxyInterface) realmObject;
        RealmRouteRealmProxyInterface realmObjectSource = (RealmRouteRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$origin(realmObjectSource.realmGet$origin());
        realmObjectTarget.realmSet$destination(realmObjectSource.realmGet$destination());
        realmObjectTarget.realmSet$routeString(realmObjectSource.realmGet$routeString());
        realmObjectTarget.realmSet$timeQueried(realmObjectSource.realmGet$timeQueried());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("RealmRoute = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{origin:");
        stringBuilder.append(realmGet$origin() != null ? realmGet$origin() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{destination:");
        stringBuilder.append(realmGet$destination() != null ? realmGet$destination() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{routeString:");
        stringBuilder.append(realmGet$routeString() != null ? realmGet$routeString() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{timeQueried:");
        stringBuilder.append(realmGet$timeQueried() != null ? realmGet$timeQueried() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RealmRouteRealmProxy aRealmRoute = (RealmRouteRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aRealmRoute.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aRealmRoute.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aRealmRoute.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
