package com.carpolo.roaa.carpolo_test.models;

import com.carpolo.roaa.carpolo_test.classes.Point;
import com.carpolo.roaa.carpolo_test.realm_objects.RealmRoute;

import java.util.Date;

import io.realm.Realm;

/**
 *
 * Class that stores and retrieves routes from Realm
 *
 */
public class RouteModel {

    /**
     * Function that inserts a route between two points into Realm
     * @param origin origin point of the route
     * @param destination destination point of the route
     * @param routeString Google Maps Directions API route
     */
    public void insertRoute(final Point origin, final Point destination, final String routeString, final Date timeQueried) {

        System.out.println("inserting route");

        Realm realm = Realm.getDefaultInstance();

        try {
            realm.executeTransaction(new Realm.Transaction() {

                @Override
                public void execute(Realm realm) {

                    // auto-increment the id
                    Number latestId = realm.where(RealmRoute.class).max("id");
                    long nextId = (latestId == null) ? 1 : latestId.intValue() + 1;

                    String originCoordinates = origin.getLatitude() + "," + origin.getLongitude();
                    String destinationCoordinates = destination.getLatitude() + "," + destination.getLongitude();

                    // create RealmPoint and set its values
                    RealmRoute route = realm.createObject(RealmRoute.class, nextId);
                    route.setOrigin(originCoordinates);
                    route.setDestination(destinationCoordinates);
                    route.setRouteString(routeString);
                    route.setTimeQueried(timeQueried);

                    realm.insertOrUpdate(route); // insert point into Realm, update if exists
                    System.out.println("route successfully inserted");
                }
            });
        } catch (Exception e){
            System.out.println("route not inserted");
            e.printStackTrace();
        } finally{
            if (realm != null) {
                realm.close();
            }
        }
    }

    /**
     * Function that gets route between origin and destination stored in Realm
     * @param origin origin point of the route
     * @param destination destination point of the route
     * @return route if found, empty string if none
     */
    public String getRoute(final Point origin, final Point destination){

        String route = "";
        String originCoordinates = origin.getLatitude() + "," + origin.getLongitude();
        String destinationCoordinates = destination.getLatitude() + "," + destination.getLongitude();

        Realm realm = Realm.getDefaultInstance();

        try {

            final RealmRoute realmRoute = realm.where(RealmRoute.class)
                    .equalTo("origin", originCoordinates)
                    .equalTo("destination", destinationCoordinates).findFirst();

            if(realmRoute != null)
                route = realmRoute.getRouteString();

        }catch (Exception e){
            e.printStackTrace();
        } finally{
            if (realm != null) {
                realm.close();
            }
        }
        return route;
    }
}
