package com.carpolo.roaa.carpolo_test.realm_objects;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 *
 * Class that defines the Route schema in Realm
 *
 */
public class RealmRoute extends RealmObject {

    @PrimaryKey
    private long id; // auto-increment
    private String origin; // coordinates of the origin point
    private String destination; // coordinates of the destination point
    private String routeString; // Google Maps Directions API route
    private Date timeQueried; // time when Google Maps Directions was queried for route

    public RealmRoute(){

    }

    public RealmRoute(long id, String origin, String destination, String routeString) {
        this.id = id;
        this.origin = origin;
        this.destination = destination;
        this.routeString = routeString;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getRouteString() {
        return routeString;
    }

    public void setRouteString(String routeString) {
        this.routeString = routeString;
    }

    public Date getTimeQueried() {
        return timeQueried;
    }

    public void setTimeQueried(Date timeQueried) {
        this.timeQueried = timeQueried;
    }
}