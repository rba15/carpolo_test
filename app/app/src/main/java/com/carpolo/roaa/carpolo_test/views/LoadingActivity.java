package com.carpolo.roaa.carpolo_test.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.carpolo.roaa.carpolo_test.R;
import com.carpolo.roaa.carpolo_test.controllers.PointController;
import com.carpolo.roaa.carpolo_test.controllers.RealmController;
import com.carpolo.roaa.carpolo_test.controllers.RouteController;

/**
 * Launcher Activity
 * Displays app icon and loads the map activity
 */

public class LoadingActivity extends Activity {

    public static PointController pointController;
    public static RouteController routeController;
    RealmController realmController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realmController = new RealmController(getApplicationContext());
        pointController = new PointController();
        routeController = new RouteController();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(getApplicationContext(),MapsActivity.class);

                // set points and route in MapsActivity
                MapsActivity.points = pointController.getPoints();
                MapsActivity.route = routeController.getRoute(MapsActivity.points.get(0),MapsActivity.points.get(1));
                startActivity(i);
                finish();
            }
        }, 4000);
    }
}
