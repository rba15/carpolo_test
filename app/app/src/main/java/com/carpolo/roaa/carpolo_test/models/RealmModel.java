package com.carpolo.roaa.carpolo_test.models;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmModel {

    private Context appContext;

    public RealmModel(Context appContext) {
        this.appContext = appContext;
        initRealm();
    }

    public void initRealm(){

        Realm.init(this.appContext);
        RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);
    }
}
