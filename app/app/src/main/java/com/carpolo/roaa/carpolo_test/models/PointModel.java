package com.carpolo.roaa.carpolo_test.models;

import com.carpolo.roaa.carpolo_test.classes.Point;
import com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 *
 * Class that stores and retrieves points from Realm
 *
 */
public class PointModel {

    /**
     * Function that inserts a point into Realm
     * @param p point to be inserted
     */
    public void insertPoint(final Point p) {

        Realm realm = Realm.getDefaultInstance();

        try {
            realm.executeTransaction(new Realm.Transaction() {

                @Override
                public void execute(Realm realm) {

                    String coordinates = p.getLatitude() + "," + p.getLongitude(); // create primary key

                    // create RealmPoint and set its values
                    RealmPoint point = realm.createObject(RealmPoint.class, coordinates);
                    point.setName(p.getName());
                    point.setLatitude(p.getLatitude());
                    point.setLongitude(p.getLongitude());

                    realm.insertOrUpdate(point); // insert point into Realm, update if exists
                    System.out.println("point successfully inserted");
                }
            });
        } catch (Exception e){
            e.printStackTrace();
        } finally{
            if (realm != null) {
                realm.close();
            }
        }
    }

    /**
     * Function that fetches all points stored in Realm
     * @return ArrayList<Point> of points in Realm
     */
    public ArrayList<Point> getPoints(){

        Realm realm = Realm.getDefaultInstance();

        ArrayList<Point> points = new ArrayList<Point>();

        try {

            // get all points in Realm
            final RealmResults<RealmPoint> realmPoints = realm.where(RealmPoint.class).findAll();
            for (RealmPoint p : realmPoints) {
                System.out.println(p);
                points.add(new Point(p.getName(), p.getLatitude(), p.getLongitude())); // append to arraylist of Points
            }
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            if(realm != null)
                realm.close();
        }

        return points;
    }
}