package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class RealmPointRealmProxy extends com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint
    implements RealmObjectProxy, RealmPointRealmProxyInterface {

    static final class RealmPointColumnInfo extends ColumnInfo {
        long coordinatesIndex;
        long latitudeIndex;
        long longitudeIndex;
        long nameIndex;

        RealmPointColumnInfo(OsSchemaInfo schemaInfo) {
            super(4);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("RealmPoint");
            this.coordinatesIndex = addColumnDetails("coordinates", objectSchemaInfo);
            this.latitudeIndex = addColumnDetails("latitude", objectSchemaInfo);
            this.longitudeIndex = addColumnDetails("longitude", objectSchemaInfo);
            this.nameIndex = addColumnDetails("name", objectSchemaInfo);
        }

        RealmPointColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new RealmPointColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final RealmPointColumnInfo src = (RealmPointColumnInfo) rawSrc;
            final RealmPointColumnInfo dst = (RealmPointColumnInfo) rawDst;
            dst.coordinatesIndex = src.coordinatesIndex;
            dst.latitudeIndex = src.latitudeIndex;
            dst.longitudeIndex = src.longitudeIndex;
            dst.nameIndex = src.nameIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(4);
        fieldNames.add("coordinates");
        fieldNames.add("latitude");
        fieldNames.add("longitude");
        fieldNames.add("name");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private RealmPointColumnInfo columnInfo;
    private ProxyState<com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint> proxyState;

    RealmPointRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (RealmPointColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$coordinates() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.coordinatesIndex);
    }

    @Override
    public void realmSet$coordinates(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'coordinates' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public double realmGet$latitude() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.latitudeIndex);
    }

    @Override
    public void realmSet$latitude(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.latitudeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.latitudeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public double realmGet$longitude() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.longitudeIndex);
    }

    @Override
    public void realmSet$longitude(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.longitudeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.longitudeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$name() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nameIndex);
    }

    @Override
    public void realmSet$name(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.nameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.nameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.nameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.nameIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("RealmPoint", 4, 0);
        builder.addPersistedProperty("coordinates", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("latitude", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("longitude", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("name", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static RealmPointColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new RealmPointColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "RealmPoint";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint obj = null;
        if (update) {
            Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
            RealmPointColumnInfo columnInfo = (RealmPointColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
            long pkColumnIndex = columnInfo.coordinatesIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("coordinates")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("coordinates"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class), false, Collections.<String> emptyList());
                    obj = new io.realm.RealmPointRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("coordinates")) {
                if (json.isNull("coordinates")) {
                    obj = (io.realm.RealmPointRealmProxy) realm.createObjectInternal(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.RealmPointRealmProxy) realm.createObjectInternal(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class, json.getString("coordinates"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'coordinates'.");
            }
        }

        final RealmPointRealmProxyInterface objProxy = (RealmPointRealmProxyInterface) obj;
        if (json.has("latitude")) {
            if (json.isNull("latitude")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'latitude' to null.");
            } else {
                objProxy.realmSet$latitude((double) json.getDouble("latitude"));
            }
        }
        if (json.has("longitude")) {
            if (json.isNull("longitude")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'longitude' to null.");
            } else {
                objProxy.realmSet$longitude((double) json.getDouble("longitude"));
            }
        }
        if (json.has("name")) {
            if (json.isNull("name")) {
                objProxy.realmSet$name(null);
            } else {
                objProxy.realmSet$name((String) json.getString("name"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint obj = new com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint();
        final RealmPointRealmProxyInterface objProxy = (RealmPointRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("coordinates")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$coordinates((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$coordinates(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("latitude")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$latitude((double) reader.nextDouble());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'latitude' to null.");
                }
            } else if (name.equals("longitude")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$longitude((double) reader.nextDouble());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'longitude' to null.");
                }
            } else if (name.equals("name")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$name((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$name(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'coordinates'.");
        }
        return realm.copyToRealm(obj);
    }

    public static com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint copyOrUpdate(Realm realm, com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint) cachedRealmObject;
        }

        com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
            RealmPointColumnInfo columnInfo = (RealmPointColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
            long pkColumnIndex = columnInfo.coordinatesIndex;
            String value = ((RealmPointRealmProxyInterface) object).realmGet$coordinates();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, value);
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.RealmPointRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint copy(Realm realm, com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint realmObject = realm.createObjectInternal(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class, ((RealmPointRealmProxyInterface) newObject).realmGet$coordinates(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        RealmPointRealmProxyInterface realmObjectSource = (RealmPointRealmProxyInterface) newObject;
        RealmPointRealmProxyInterface realmObjectCopy = (RealmPointRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$latitude(realmObjectSource.realmGet$latitude());
        realmObjectCopy.realmSet$longitude(realmObjectSource.realmGet$longitude());
        realmObjectCopy.realmSet$name(realmObjectSource.realmGet$name());
        return realmObject;
    }

    public static long insert(Realm realm, com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
        long tableNativePtr = table.getNativePtr();
        RealmPointColumnInfo columnInfo = (RealmPointColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
        long pkColumnIndex = columnInfo.coordinatesIndex;
        String primaryKeyValue = ((RealmPointRealmProxyInterface) object).realmGet$coordinates();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        Table.nativeSetDouble(tableNativePtr, columnInfo.latitudeIndex, rowIndex, ((RealmPointRealmProxyInterface) object).realmGet$latitude(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.longitudeIndex, rowIndex, ((RealmPointRealmProxyInterface) object).realmGet$longitude(), false);
        String realmGet$name = ((RealmPointRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
        long tableNativePtr = table.getNativePtr();
        RealmPointColumnInfo columnInfo = (RealmPointColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
        long pkColumnIndex = columnInfo.coordinatesIndex;
        com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint object = null;
        while (objects.hasNext()) {
            object = (com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((RealmPointRealmProxyInterface) object).realmGet$coordinates();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            Table.nativeSetDouble(tableNativePtr, columnInfo.latitudeIndex, rowIndex, ((RealmPointRealmProxyInterface) object).realmGet$latitude(), false);
            Table.nativeSetDouble(tableNativePtr, columnInfo.longitudeIndex, rowIndex, ((RealmPointRealmProxyInterface) object).realmGet$longitude(), false);
            String realmGet$name = ((RealmPointRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
        long tableNativePtr = table.getNativePtr();
        RealmPointColumnInfo columnInfo = (RealmPointColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
        long pkColumnIndex = columnInfo.coordinatesIndex;
        String primaryKeyValue = ((RealmPointRealmProxyInterface) object).realmGet$coordinates();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        }
        cache.put(object, rowIndex);
        Table.nativeSetDouble(tableNativePtr, columnInfo.latitudeIndex, rowIndex, ((RealmPointRealmProxyInterface) object).realmGet$latitude(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.longitudeIndex, rowIndex, ((RealmPointRealmProxyInterface) object).realmGet$longitude(), false);
        String realmGet$name = ((RealmPointRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
        long tableNativePtr = table.getNativePtr();
        RealmPointColumnInfo columnInfo = (RealmPointColumnInfo) realm.getSchema().getColumnInfo(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint.class);
        long pkColumnIndex = columnInfo.coordinatesIndex;
        com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint object = null;
        while (objects.hasNext()) {
            object = (com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((RealmPointRealmProxyInterface) object).realmGet$coordinates();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            }
            cache.put(object, rowIndex);
            Table.nativeSetDouble(tableNativePtr, columnInfo.latitudeIndex, rowIndex, ((RealmPointRealmProxyInterface) object).realmGet$latitude(), false);
            Table.nativeSetDouble(tableNativePtr, columnInfo.longitudeIndex, rowIndex, ((RealmPointRealmProxyInterface) object).realmGet$longitude(), false);
            String realmGet$name = ((RealmPointRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
            }
        }
    }

    public static com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint createDetachedCopy(com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint) cachedObject.object;
            }
            unmanagedObject = (com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        RealmPointRealmProxyInterface unmanagedCopy = (RealmPointRealmProxyInterface) unmanagedObject;
        RealmPointRealmProxyInterface realmSource = (RealmPointRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$coordinates(realmSource.realmGet$coordinates());
        unmanagedCopy.realmSet$latitude(realmSource.realmGet$latitude());
        unmanagedCopy.realmSet$longitude(realmSource.realmGet$longitude());
        unmanagedCopy.realmSet$name(realmSource.realmGet$name());

        return unmanagedObject;
    }

    static com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint update(Realm realm, com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint realmObject, com.carpolo.roaa.carpolo_test.realm_objects.RealmPoint newObject, Map<RealmModel, RealmObjectProxy> cache) {
        RealmPointRealmProxyInterface realmObjectTarget = (RealmPointRealmProxyInterface) realmObject;
        RealmPointRealmProxyInterface realmObjectSource = (RealmPointRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$latitude(realmObjectSource.realmGet$latitude());
        realmObjectTarget.realmSet$longitude(realmObjectSource.realmGet$longitude());
        realmObjectTarget.realmSet$name(realmObjectSource.realmGet$name());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("RealmPoint = proxy[");
        stringBuilder.append("{coordinates:");
        stringBuilder.append(realmGet$coordinates() != null ? realmGet$coordinates() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{latitude:");
        stringBuilder.append(realmGet$latitude());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{longitude:");
        stringBuilder.append(realmGet$longitude());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{name:");
        stringBuilder.append(realmGet$name() != null ? realmGet$name() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RealmPointRealmProxy aRealmPoint = (RealmPointRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aRealmPoint.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aRealmPoint.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aRealmPoint.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
