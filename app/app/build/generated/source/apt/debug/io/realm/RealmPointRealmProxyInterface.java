package io.realm;


public interface RealmPointRealmProxyInterface {
    public String realmGet$coordinates();
    public void realmSet$coordinates(String value);
    public double realmGet$latitude();
    public void realmSet$latitude(double value);
    public double realmGet$longitude();
    public void realmSet$longitude(double value);
    public String realmGet$name();
    public void realmSet$name(String value);
}
